package tobiass.youfone;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter implements OnClickListener {
	private String[] mData;
	private Context mContext;
	private SparseArray<View> mViews;
	private int mActive;
	
	public int getActive() {
		return mActive;
	}
	
	public DrawerAdapter(Context context, String[] data) {
		mContext = context;
		mData = data;
		mViews = new SparseArray<View>(data.length);
	}
	
	public void setData(int position, String data) {
		mData[position] = data;
		notifyDataSetChanged();
	}
	
	public int getCount() {
		return mData.length;
	}

	public Object getItem(int arg0) {
		return null;
	}

	public long getItemId(int arg0) {
		return 0;
	}
	
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		TextView textView;
		if(arg1 != null) {
			textView = (TextView) arg1;
			mViews.delete((Integer) arg1.getTag());
		}
		else
			textView = (TextView) View.inflate(mContext, R.layout.drawer_item, null);
		
		textView.setText(mData[arg0]);
		textView.setBackgroundResource(arg0 == mActive ? R.drawable.drawer_item_bg_highlighted : R.drawable.drawer_item_bg);
		textView.setOnClickListener(this);
		textView.setTag(arg0);
		mViews.put(arg0, textView);
		return textView;
	}

	public void onClick(View arg0) {
		//setActive((Integer) arg0.getTag());
		if(mOnClickListener != null)
			mOnClickListener.onClick(arg0);
	}
	
	public void setActive(int index) {
		mActive = index;
		for(int i = 0; i < mViews.size(); i++) {
			View v = mViews.valueAt(i);
			v.setBackgroundResource(((Integer)v.getTag()).intValue() == mActive ? R.drawable.drawer_item_bg_highlighted : R.drawable.drawer_item_bg);
		}
	}
	
	private OnClickListener mOnClickListener;
	public DrawerAdapter setOnClickListener(OnClickListener listener) {
		mOnClickListener = listener;
		return this;
	}
}
