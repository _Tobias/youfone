package tobiass.youfone;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import tobiass.youfone.misc.EncryptedPrefs;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public abstract class YFRequest extends AsyncTask<String, Void, YFRequest.YFResponse> {
	static final Pattern LOGIN_COOKIE_PATTERN = Pattern.compile("yfk=([a-f0-9]+)");
	EncryptedPrefs mPrefs;
	Context mContext;
	
	public YFRequest(Context context) {
		mContext = context;
		mPrefs = new EncryptedPrefs(context);
	}
	
	protected YFResponse doInBackground(String... params) {
		YFResponse response = new YFResponse();
        int nr = mPrefs.unencrypted().getInt(Const.PREF_COOKIE_NR, 0);
		String cookie = mPrefs.getString(Const.PREF_COOKIE, null);
		String email = mPrefs.getString(Const.PREF_EMAIL, null);
		String password = mPrefs.getString(Const.PREF_PASSWORD, null);
		try {
			// No cookie found yet.
			if(cookie == null) {
				cookie = login(email, password);
				if(cookie == null) { // Failed getting cookie? (net errors will throw ioexception)
					response.invalidLogin = true;
					return response;
				}
			}
			
			URL url = new URL(params[0]);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			configRequest(conn, params, cookie, nr);
			if(conn.getResponseCode() == 302) { // Cookie doesn't work?
				cookie = login(email, password);
				if(cookie != null) { // Cookie works now. Retry.
					conn.disconnect();
					conn = (HttpsURLConnection) url.openConnection();
					configRequest(conn, params, cookie, nr);
				}
				else {
					conn.disconnect();
					response.invalidLogin = true;
					return response;
				}
			}
			
			if(conn.getResponseCode() == 302)
				response.invalidLogin = true;
			else {
				mPrefs.putString(Const.PREF_COOKIE, cookie).commit();
				response.html = convertStream(conn.getInputStream());
			}
			
			conn.disconnect();
			return response;
		} catch (IOException e) {
			response.networkProblem = true;
		}
		
		if(response.invalidLogin)
			mPrefs.remove(Const.PREF_COOKIE).commit();
		return response;
	}
	
	private void configRequest(HttpsURLConnection conn, String[] params, String cookie, int nr) throws IOException {
		conn.addRequestProperty("Cookie", "yfk=" + cookie + "; nr=" + nr);
        conn.setInstanceFollowRedirects(false);
		if(params.length == 2) {
			conn.setDoOutput(true);
			conn.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestMethod("POST");
			OutputStream os = conn.getOutputStream();
			os.write(params[1].getBytes());
			os.flush();
			os.close();
		}
	}
	
	public abstract void onPostExecute(YFResponse response);
	
	public static String login(String email, String password) throws IOException {
		HttpsURLConnection conn = (HttpsURLConnection) new URL(Const.LOGIN_URL).openConnection();
		conn.setRequestMethod("POST");
		conn.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setInstanceFollowRedirects(false);
		conn.setDoOutput(true);
		OutputStream os = conn.getOutputStream();
		os.write(String.format("inlognaam=%s&passwoord=%s", URLEncoder.encode(email, Const.CHARSET), URLEncoder.encode(password, Const.CHARSET)).getBytes(Const.CHARSET));
		os.flush();
		os.close();
		List<String> cookies = conn.getHeaderFields().get("Set-Cookie");
		
		conn.disconnect();
		
		if(cookies != null) {
			for(String s : cookies) {
				Matcher m = LOGIN_COOKIE_PATTERN.matcher(s);
				if(m.find()) {
					String cookie = m.group(1);
					conn = (HttpsURLConnection) new URL(Const.LOGIN_URL).openConnection();
					conn.setInstanceFollowRedirects(false);
					conn.addRequestProperty("Cookie", "yfk=" + cookie);
					boolean valid = conn.getResponseCode() == 200;
					conn.disconnect();
					return valid ? cookie : null;
					
				}
			}
		}
		return null;
	}
	
	private static String convertStream(InputStream is) {
	    Scanner s = new Scanner(is, "UTF-8");
	    s.useDelimiter("\\A");
	    String r = s.hasNext() ? s.next() : "";
	    s.close();
	    return r;
	}
	
	public class YFResponse {
		public String html;
		public boolean networkProblem;
		public boolean invalidLogin;
		
		public String toString() {
			if(networkProblem)
				return mContext.getString(R.string.error_connecting);
			else if(invalidLogin)
				return mContext.getString(R.string.error_invalid_login);
			else
				return "Response OK";
		}
	}
}
