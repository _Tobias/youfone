package tobiass.youfone;

import java.util.regex.Pattern;

public class Const {
	public static final String PREF_COOKIE = "a";
	public static final String PREF_EMAIL = "b";
	public static final String PREF_PASSWORD = "c";
    public static final String PREF_COOKIE_NR = "d";
	public static final String STATE_FRAGMENT = "a";
	public static final String STATE_HTML = "b";
	public static final String ARGUMENT_MESSAGE = "a";
	public static final String ARGUMENT_BUTTON_TITLE = "b";
	public static final String ARGUMENT_BUTTON_ACTION = "c";
	public static final int ACTION_LOGOUT = 1;
	public static final String LOGIN_URL = "https://www.youfone.nl/myYF/page.php";
	public static final String TAG = "Youfone";
	public static final Pattern EMAIL_VALIDATOR = Pattern.compile("^[a-zA-Z0-9.!#$%&amp;'*+-/=?\\^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$");
	public static final Pattern HTML_TAG_TRIM = Pattern.compile("</?[a-z]+>");
	public static final Pattern TD_ROW_PATTERN = Pattern.compile("<td class=\"tdrow\">(.*?)</td>");
	public static final String CHARSET = "UTF-8";
}
