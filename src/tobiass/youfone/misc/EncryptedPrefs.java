package tobiass.youfone.misc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class EncryptedPrefs {
	SharedPreferences mPrefs;
	Editor mEditor;
	Crypto mCrypto;
	static final String KEY = "}St5LK!`495hVvK*fh]4d<t.l8;e2x7h-Ex7'E'kYr:w6&y0Zn'HELLO:)";

    @SuppressLint("CommitPrefEdits")
    public EncryptedPrefs(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPrefs.edit();
        mCrypto = new Crypto(KEY);
    }

    public EncryptedPrefs putString(String key, String value) {
        try {
            mEditor.putString(key, mCrypto.encrypt(value));
        } catch (Exception e) {}
        return this;
    }

    public String getString(String key, String def) {
        try {
            return mCrypto.decrypt(mPrefs.getString(key, null));
        } catch (Exception e) {}
        return null;
    }

    public EncryptedPrefs remove(String key) {
        mEditor.remove(key);
        return this;
    }

    public SharedPreferences unencrypted() {
        return mPrefs;
    }

    public boolean contains(String key) {
        return mPrefs.contains(key);
    }

    public Editor edit() {
        return mEditor;
    }

    public boolean commit() {
        return mEditor.commit();
    }
}
