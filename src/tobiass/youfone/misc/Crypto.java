package tobiass.youfone.misc;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import tobiass.youfone.Const;
import android.util.Base64;

public class Crypto {
	SecretKey key;
	public Crypto(String seed) {
		DESKeySpec keySpec;
		try {
			keySpec = new DESKeySpec(seed.getBytes(Const.CHARSET));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			key = keyFactory.generateSecret(keySpec);
		}
		catch (Exception e) {}
	}
	
	public String encrypt(String text) {
		try {
			byte[] cleartext = text.getBytes(Const.CHARSET);

			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return Base64.encodeToString(cipher.doFinal(cleartext), Base64.DEFAULT);
		}
		catch (Exception e) {}
		return null;
	}
	
	public String decrypt(String encrypted) {
		try {
			byte[] enc = Base64.decode(encrypted, Base64.DEFAULT);
			
			Cipher cipher = Cipher.getInstance("DES");// cipher is not thread safe
			cipher.init(Cipher.DECRYPT_MODE, key);
			
			return new String(cipher.doFinal(enc), Const.CHARSET);
		}
		catch (Exception e) {}
		return null;
	}
}