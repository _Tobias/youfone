package tobiass.youfone;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Static {
	public static void dockKeyboard(Activity activity) {
		((InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
	}
	
	public static void dockKeyboard(Object imm, IBinder windowToken) {
		((InputMethodManager)imm).hideSoftInputFromWindow(windowToken, 0);
	}
	
	public static void addToVG(ViewGroup group, String key, String value) {
		Context context = group.getContext();
		key = Const.HTML_TAG_TRIM.matcher(key).replaceAll("");
		TextView keyText = new TextView(context);
		keyText.setText(key);
		
		TextView valueText = null;
		if(value != null) {
			value = Const.HTML_TAG_TRIM.matcher(value).replaceAll("");
			valueText = new TextView(context);
			valueText.setText(value);
		}
		if(group instanceof TableLayout) {
			TableRow row = new TableRow(context);
			row.addView(keyText);
			if(valueText != null)
				row.addView(valueText);
			else
				keyText.setTextAppearance(context, R.style.HeaderText);
			group.addView(row);
		}
		else {
			group.addView(keyText);
			if(valueText != null) {
				keyText.setTextAppearance(context, R.style.HeaderText);
				group.addView(valueText);
			}
			else
				keyText.setTextAppearance(context, R.style.BiggerHeaderText);
		}
	}
}
