package tobiass.youfone;

import tobiass.youfone.YFRequest.YFResponse;
import tobiass.youfone.fragments.AccountFragment;
import tobiass.youfone.fragments.ContactFragment;
import tobiass.youfone.fragments.MessageFragment;
import tobiass.youfone.fragments.LoginFragment;
import tobiass.youfone.fragments.StatsFragment;
import tobiass.youfone.fragments.SubSelectorFragment;
import tobiass.youfone.fragments.SubscriptionFragment;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Main extends ActionBarActivity implements OnClickListener {
    DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    public DrawerAdapter mDrawerAdapter;
    ActionBarDrawerToggle mDrawerToggle;
    SharedPreferences mPrefs;
    AdView adView;
    public static final int FRAGMENT_SIGNIN = 7;
    
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.drawer_layout);
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setFocusableInTouchMode(false);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        
        adView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder()
            .addTestDevice("084BCF0DDFC684389CBF00EB90DD8C84")
            .build();
        adView.loadAd(adRequest);
        
        int activeFragment = FRAGMENT_SIGNIN;
        
        String[] items = getResources().getStringArray(R.array.drawer_items);
        if(mPrefs.contains(Const.PREF_EMAIL)) {
        	items[FRAGMENT_SIGNIN] = getString(R.string.sign_off);
        	activeFragment = savedInstanceState != null ? savedInstanceState.getInt(Const.STATE_FRAGMENT) : 0;
        }
        
        mDrawerAdapter = new DrawerAdapter(this, items).setOnClickListener(this);
        mDrawerList.setAdapter(mDrawerAdapter);
        
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        loadFragment(activeFragment);
	}
	
	public void onResume() {
		super.onResume();
		if(adView != null) {
			adView.resume();
		}
	}

	public void onPause() {
		if(adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	public void onDestroy() {
		if(adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
	
	public void onSaveInstanceState(Bundle out) {
		super.onSaveInstanceState(out);
		out.putInt(Const.STATE_FRAGMENT, mDrawerAdapter.getActive());
	}
	
	public void showError(int error, String buttonText, int action) {
		Fragment f = new MessageFragment();
		Bundle args = new Bundle();
		args.putInt(Const.ARGUMENT_MESSAGE, error);
		if(buttonText != null)
			args.putString(Const.ARGUMENT_BUTTON_TITLE, buttonText);
		if(action != -1)
			args.putInt(Const.ARGUMENT_BUTTON_ACTION, action);
		f.setArguments(args);
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.content_frame, f)
		.commit();
		
		mDrawerAdapter.setActive(-1);
	}
	
	public void showError(int error) {
		showError(error, null, -1);
	}
	
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
	
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
    static StatsFragment mStatsFragment = new StatsFragment();
    static SubscriptionFragment mSubscriptionFragment = new SubscriptionFragment();
    static AccountFragment mAccountFragment = new AccountFragment();
    static SubSelectorFragment mSubSelectorFragment = new SubSelectorFragment();
    static ContactFragment mContactFragment = new ContactFragment();
    static LoginFragment mLoginFragment = new LoginFragment();
    static MessageFragment mAboutFragment = new MessageFragment();
    static {
    	Bundle args = new Bundle();
    	args.putInt(Const.ARGUMENT_MESSAGE, R.string.about);
    	mAboutFragment.setArguments(args);
    }

	public void onClick(View v) {		
		mDrawerLayout.closeDrawers();
		int index = (Integer) v.getTag();
		
		if(mDrawerAdapter.getActive() != index)
			loadFragment(index);
	}
	
	public void loadFragment(int index) {
		boolean loggedIn = mPrefs.contains(Const.PREF_EMAIL);
		if(!loggedIn && index != FRAGMENT_SIGNIN)
			return;
		
		Fragment f = null;
		switch(index) {
		case 0:
			f = mStatsFragment;
			break;
		case 1:
			f = mSubscriptionFragment;
			break;
		case 2:
			f = mAccountFragment;
			break;
		case 3:
			f = mContactFragment;
			break;
		case 4:
			f = mAboutFragment;
			break;
        case 5:
            f = mSubSelectorFragment;
            break;
        case 6:
            Uri uri = Uri.parse("market://details?id=com.youfone");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.youfone")));
            }
            return;
		case FRAGMENT_SIGNIN:
			if(loggedIn) {
				new AlertDialog.Builder(this)
				.setMessage(R.string.sign_off_confirmation)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						logout();
					}
				})
				.setNegativeButton(R.string.no, null)
				.show();
				return;
			}
			else
				f = mLoginFragment;
			break;
		}

        if(f == null)
            return;
		
		getSupportFragmentManager().beginTransaction()
		.replace(R.id.content_frame, f)
		.commit();
		
		Static.dockKeyboard(this);
		
		mDrawerAdapter.setActive(index);
	}
	
	public boolean handleResponse(YFResponse response) {
		if(response.invalidLogin) {
			Toast.makeText(this, R.string.message_session_expired, Toast.LENGTH_LONG).show();
			logout();
			return true;
		}
		else if(response.networkProblem) {
			showError(R.string.error_connecting);
			return true;
		}
		return false;
	}
	
	public void logout() {
		mPrefs.edit()
		.remove(Const.PREF_COOKIE)
		.remove(Const.PREF_EMAIL)
		.remove(Const.PREF_PASSWORD)
		.commit();
		
		mDrawerAdapter.setActive(FRAGMENT_SIGNIN);
		mDrawerAdapter.setData(FRAGMENT_SIGNIN, getString(R.string.sign_in));

		getSupportFragmentManager().beginTransaction()
		.replace(R.id.content_frame, mLoginFragment)
		.commit();

        mStatsFragment.reset();
        mSubscriptionFragment.reset();
        mAccountFragment.reset();
        mContactFragment.reset();
	}

    public void reload() {
        mStatsFragment.reset();
        mSubscriptionFragment.reset();
        mAccountFragment.reset();
        mContactFragment.reset();

        loadFragment(0);
    }

    public void onBackPressed() {
        View drawer = mDrawerLayout.getChildAt(1);
        if(!mDrawerLayout.isDrawerVisible(drawer))
            mDrawerLayout.openDrawer(drawer);
        else
            super.onBackPressed();
    }
}