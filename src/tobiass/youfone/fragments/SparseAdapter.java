package tobiass.youfone.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SparseAdapter extends BaseAdapter {
	Context mContext;
	List<String> mStringData = new ArrayList<String>();
	public SparseAdapter(Context context, SparseArray<String> array) {
		mContext = context;
		for(int i = 0; i < array.size(); i++)
			mStringData.add(array.valueAt(i));
	}
	
	public SparseAdapter(Context context, List<Long> array) {
		mContext = context;
		for(int i = 0; i < array.size(); i++)
			mStringData.add(String.valueOf(array.get(i)));
	}
	
	public SparseAdapter(Context context, SparseIntArray array) {
		mContext = context;
		for(int i = 0; i < array.size(); i++)
			mStringData.add(String.valueOf(array.valueAt(i)));
	}

	public int getCount() {
		return mStringData.size();
	}

	public Object getItem(int arg0) {
		return mStringData.get(arg0);
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		final TextView t = arg1 == null ? (TextView) View.inflate(mContext, android.R.layout.simple_list_item_1, null) : (TextView) arg1;
		t.setText(mStringData.get(arg0));
		t.post(new Runnable() {
			public void run() {
				t.setSingleLine(false);
			}
		});
		return t;
	}

}
