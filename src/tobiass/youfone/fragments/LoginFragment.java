package tobiass.youfone.fragments;

import java.io.IOException;

import tobiass.youfone.Const;
import tobiass.youfone.ErrorDismissWatcher;
import tobiass.youfone.Main;
import tobiass.youfone.R;
import tobiass.youfone.Static;
import tobiass.youfone.YFRequest;
import tobiass.youfone.misc.EncryptedPrefs;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginFragment extends Fragment implements OnClickListener {
	ViewGroup mViewGroup;
	EditText mEmail;
	EditText mPassword;
	TextView mError;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		mViewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_login, null);
		Button signinButton = (Button) mViewGroup.findViewById(R.id.input_sign_in);
		signinButton.setOnClickListener(this);
		
		mEmail = (EditText) mViewGroup.findViewById(R.id.input_email);
		mEmail.addTextChangedListener(new ErrorDismissWatcher(mEmail));
		mPassword = (EditText) mViewGroup.findViewById(R.id.input_password);
		mPassword.addTextChangedListener(new ErrorDismissWatcher(mPassword));
		mError = (TextView) mViewGroup.findViewById(R.id.error);
		return mViewGroup;
	}
	
	private class LoginTask extends AsyncTask<String, Void, Void> {
		private boolean IOException;
		private boolean invalidLogin;
		private ProgressDialog mDialog;
		
		protected void onPreExecute() {
			mDialog = ProgressDialog.show(getActivity(), null, getText(R.string.signing_in));
			mError.setText(null);
			Static.dockKeyboard(getActivity());
		}
		
		protected Void doInBackground(String... params) {
			try {
				String cookie = YFRequest.login(params[0], params[1]);
				if(cookie == null)
					invalidLogin = true;
				else {
					new EncryptedPrefs(getActivity())
					.putString(Const.PREF_EMAIL, params[0])
					.putString(Const.PREF_PASSWORD, params[1])
					.putString(Const.PREF_COOKIE, cookie)
					.commit();
				}
			} catch (IOException e) {
				IOException = true;
			}
			return null;
		}
		
		protected void onPostExecute(Void result) {
			mDialog.cancel();
			if(IOException)
				mError.setText(R.string.error_connecting);
			else if(invalidLogin)
				mError.setText(R.string.error_invalid_login);
			else {
				Main activity = (Main) getActivity();
				activity.mDrawerAdapter.setData(Main.FRAGMENT_SIGNIN, getString(R.string.sign_off));
				activity.loadFragment(0);
			}
		}
	}

	public void onClick(View v) {
		String email = mEmail.getText().toString();
		String password = mPassword.getText().toString();
		if(!Const.EMAIL_VALIDATOR.matcher(email).matches()) {
			mEmail.setError(getActivity().getString(R.string.error_invalid_email));
			return;
		}
		if(password.length() == 0) {
			mPassword.setError(getActivity().getString(R.string.error_invalid_password));
			return;
		}
		new LoginTask().execute(email, password);
		mPassword.setText(null);
	}
}
