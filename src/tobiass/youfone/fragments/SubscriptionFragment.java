package tobiass.youfone.fragments;

import java.util.regex.Matcher;

import tobiass.youfone.Const;
import tobiass.youfone.Main;
import tobiass.youfone.R;
import tobiass.youfone.Static;
import tobiass.youfone.YFRequest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewSwitcher;

public class SubscriptionFragment extends Fragment implements OnMenuItemClickListener {
	ViewSwitcher mViewSwitcher;
	ViewGroup mTable;
	String mHTML;

    public void reset() {
        mHTML = null;
    }
	
	public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		mViewSwitcher = (ViewSwitcher) inflater.inflate(R.layout.fragment_table, null);
		mTable = (ViewGroup) mViewSwitcher.findViewById(R.id.table);
		if(savedInstanceState != null && savedInstanceState.containsKey(Const.STATE_HTML)) {
			loadHTML(savedInstanceState.getString(Const.STATE_HTML));
			mViewSwitcher.setDisplayedChild(1);
		}
		else if(mHTML != null) {
			loadHTML(mHTML);
			mViewSwitcher.setDisplayedChild(1);
		}
		else
			refresh();
		return mViewSwitcher;
	}
	
	private void refresh() {
		mViewSwitcher.setDisplayedChild(0);
		YFRequest req = new YFRequest(getActivity()) {
			public void onPostExecute(YFResponse response) {
				Main activity = (Main) getActivity();
				
				if(activity == null || activity.handleResponse(response))
					return;

                mViewSwitcher.setDisplayedChild(1);
				
				loadHTML(response.html);

				activity.supportInvalidateOptionsMenu();
			}
		};
		req.execute("https://www.youfone.nl/myYF/page.php?id=2");
	}
	
	public void onCreate(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
	}
	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		boolean idle = mViewSwitcher != null && mViewSwitcher.getDisplayedChild() == 1;
		MenuItem refreshItem = menu.add(R.string.refresh)
				.setIcon(R.drawable.ic_action_refresh)
				.setOnMenuItemClickListener(this);
		refreshItem.setEnabled(idle);
		refreshItem.setVisible(idle);
		
		MenuItemCompat.setShowAsAction(refreshItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
	    super.onCreateOptionsMenu(menu, inflater);
	}
	
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(Const.STATE_HTML, mHTML);
	}
	
	public void loadHTML(String html) {
		mHTML = html;
		Matcher mt = Const.TD_ROW_PATTERN.matcher(html);
		mTable.removeAllViews();
		while(mt.find()) {
			String key = Const.HTML_TAG_TRIM.matcher(mt.group(1)).replaceAll("");
			mt.find();
			String value = Const.HTML_TAG_TRIM.matcher(mt.group(1)).replaceAll("").trim();
			if(value.length() == 0 || key.equals("Belplafond") || key.equals("Status"))
				continue;
			Static.addToVG(mTable, key, value);
		}
	}

	public boolean onMenuItemClick(MenuItem item) {		
		refresh();
		getActivity().supportInvalidateOptionsMenu();
		return true;
	}
}
