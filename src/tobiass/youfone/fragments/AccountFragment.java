package tobiass.youfone.fragments;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;

import tobiass.youfone.Const;
import tobiass.youfone.ErrorDismissWatcher;
import tobiass.youfone.Main;
import tobiass.youfone.R;
import tobiass.youfone.Static;
import tobiass.youfone.YFRequest;
import tobiass.youfone.misc.EncryptedPrefs;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class AccountFragment extends Fragment implements OnMenuItemClickListener, OnClickListener, OnCancelListener {
	ViewSwitcher mViewSwitcher;
	ViewGroup mTable;
	String mHTML;

    public void reset() {
        mHTML = null;
    }
	
	public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		mViewSwitcher = (ViewSwitcher) inflater.inflate(R.layout.fragment_account, null);
		mTable = (ViewGroup) mViewSwitcher.findViewById(R.id.table);
		Button changePassword = (Button) mViewSwitcher.findViewById(R.id.input_change_password);
		changePassword.setOnClickListener(this);
		if(savedInstanceState != null && savedInstanceState.containsKey(Const.STATE_HTML)) {
			loadHTML(savedInstanceState.getString(Const.STATE_HTML));
			mViewSwitcher.setDisplayedChild(1);
		}
		else if(mHTML != null) {
			loadHTML(mHTML);
			mViewSwitcher.setDisplayedChild(1);
		}
		else
			refresh();
		return mViewSwitcher;
	}
	
	private void refresh() {
		mViewSwitcher.setDisplayedChild(0);
		YFRequest req = new YFRequest(getActivity()) {
			public void onPostExecute(YFResponse response) {
				Main activity = (Main) getActivity();
				
				if(activity == null || activity.handleResponse(response))
					return;

                mViewSwitcher.setDisplayedChild(1);
				
				loadHTML(response.html);

				isIdle = true;
				activity.supportInvalidateOptionsMenu();
			}
		};
		req.execute("https://www.youfone.nl/myYF/page.php?id=1");
	}
	
	public void onCreate(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
	}
	
	boolean isIdle;
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		MenuItem refreshItem = menu.add(R.string.refresh)
				.setIcon(R.drawable.ic_action_refresh)
				.setOnMenuItemClickListener(this);
		refreshItem.setEnabled(isIdle);
		refreshItem.setVisible(isIdle);
		
		MenuItemCompat.setShowAsAction(refreshItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
	    super.onCreateOptionsMenu(menu, inflater);
	}
	
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(Const.STATE_HTML, mHTML);
	}
	
	public void loadHTML(String html) {
		mHTML = html;
		Matcher mt = Const.TD_ROW_PATTERN.matcher(html);
		mTable.removeAllViews();
		while(mt.find()) {
			String key = Const.HTML_TAG_TRIM.matcher(mt.group(1)).replaceAll("");
			mt.find();
			String value = Const.HTML_TAG_TRIM.matcher(mt.group(1)).replaceAll("");
			if(key.equals("Huidig wachtwoord"))
				continue;
			Static.addToVG(mTable, key, value);
		}
	}

	public boolean onMenuItemClick(MenuItem item) {
		isIdle = false;
		getActivity().supportInvalidateOptionsMenu();
		
		refresh();
		return true;
	}

	// change password
	ViewGroup mDialogView;
	EditText mPass;
	EditText mPassRepeat;
	EditText mPassCurrent;
	AlertDialog mPassChangeDialog;
	public void onClick(View v) {
		// Save clicked
		if(mPassChangeDialog != null && v == mPassChangeDialog.getButton(AlertDialog.BUTTON_POSITIVE)) {			
			final String pass1 = mPass.getText().toString();
			String pass2 = mPassRepeat.getText().toString();
			String current = mPassCurrent.getText().toString();
			final EncryptedPrefs prefs = new EncryptedPrefs(getActivity());
			if(!current.equals(prefs.getString(Const.PREF_PASSWORD, null))) {
				mPassCurrent.setError(getText(R.string.error_incorrect_password));
				return;
			}
			if(pass1.length() == 0) {
				mPass.setError(getText(R.string.error_invalid_password));
				return;
			}
			else if(pass2.length() == 0) {
				mPassRepeat.setError(getText(R.string.error_invalid_password));
				return;
			}
			else if(!pass1.equals(pass2)) {
				mPassRepeat.setError(getText(R.string.error_no_match));
				return;
			}
			
			Static.dockKeyboard(getActivity().getSystemService(Context.INPUT_METHOD_SERVICE), mPass.getWindowToken());
			final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, getText(R.string.busy));
			YFRequest req = new YFRequest(getActivity()) {
				public void onPostExecute(YFResponse response) {
					progressDialog.cancel();
					
					Main activity = (Main) getActivity();
					
					if(activity == null || activity.handleResponse(response))
						return;
					
					prefs
					.putString(Const.PREF_PASSWORD, pass1)
					.remove(Const.PREF_COOKIE)
					.commit();
					
					Toast.makeText(activity, R.string.message_password_changed, Toast.LENGTH_LONG).show();
				}
			};
			try {
				req.execute("https://www.youfone.nl/myYF/page.php?id=8", String.format("ww1=%1$s&ww2=%1$s&versturen=ja", URLEncoder.encode(pass1, Const.CHARSET)));
			} catch (UnsupportedEncodingException e) {}
			
			mPassChangeDialog.cancel();
			return;
		}
		
		// Cancel clicked
		else if(mPassChangeDialog != null && v == mPassChangeDialog.getButton(AlertDialog.BUTTON_NEGATIVE))
			mPassChangeDialog.cancel();
		
		else {
			if(mPassChangeDialog == null) {
				mDialogView = (ViewGroup) getLayoutInflater(null).inflate(R.layout.dialog_change_password, null);
				mPass = (EditText) mDialogView.findViewById(R.id.input_new_password);
				mPass.addTextChangedListener(new ErrorDismissWatcher(mPass));
				mPassRepeat = (EditText) mDialogView.findViewById(R.id.input_new_password_repeat);
				mPassRepeat.addTextChangedListener(new ErrorDismissWatcher(mPassRepeat));
				mPassCurrent = (EditText) mDialogView.findViewById(R.id.input_current_password);
				mPassCurrent.addTextChangedListener(new ErrorDismissWatcher(mPassCurrent));
				
				mPassChangeDialog = new AlertDialog.Builder(getActivity())
				.setTitle(R.string.change_password)
				.setView(mDialogView)
				.setPositiveButton(R.string.save, null)
				.setNegativeButton(R.string.cancel, null)
				.setOnCancelListener(this)
				.create();
			}
			
			mPassChangeDialog.show();
			mPassChangeDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(this);
			mPassChangeDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(this);
		}
	}

	public void onCancel(DialogInterface dialog) {
		mPass.setText(null);
		mPassRepeat.setText(null);
		mPassCurrent.setText(null);
	}
}
