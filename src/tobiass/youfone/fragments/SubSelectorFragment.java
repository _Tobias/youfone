package tobiass.youfone.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tobiass.youfone.Const;
import tobiass.youfone.Main;
import tobiass.youfone.R;
import tobiass.youfone.YFRequest;

/**
 * Created by Tobias on 7/8/2014.
 */
public class SubSelectorFragment extends Fragment implements AdapterView.OnItemClickListener, MenuItem.OnMenuItemClickListener {
    Main mActivity;
    ArrayAdapter<String> mAdapter;
    ViewSwitcher mViewSwitcher;
    ListView mListView;
    SharedPreferences mPrefs;
    public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        mActivity = (Main) getActivity();

        mViewSwitcher = (ViewSwitcher) inflater.inflate(R.layout.fragment_subselector, null);
        mListView = (ListView) mViewSwitcher.findViewById(R.id.list);
        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mListView.setOnItemClickListener(this);
        TextView emptyMsg = (TextView) inflater.inflate(R.layout.message_textview, null);
        emptyMsg.setVisibility(View.GONE);
        emptyMsg.setText(R.string.message_only_one_subscription);
        ((ViewGroup)mListView.getParent()).addView(emptyMsg);
        mListView.setEmptyView(emptyMsg);
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        if(mAdapter != null) {
            mListView.setAdapter(mAdapter);
            setSelected();
            mViewSwitcher.setDisplayedChild(1);
        }
        else
            refresh();
        return mViewSwitcher;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    private void setSelected() {
        mListView.setItemChecked(mPrefs.getInt(Const.PREF_COOKIE_NR, 0), true);
    }

    static final Pattern SUBS_PATTERN = Pattern.compile("<option value=\"https://www.youfone.nl/myYF/changenr.php?.+?\" .*?>(.+?)</option>");
    private void refresh() {
        mViewSwitcher.setDisplayedChild(0);
        mActivity.supportInvalidateOptionsMenu();
        final YFRequest req = new YFRequest(mActivity) {
            public void onPostExecute(YFResponse response) {
                if(mActivity == null || mActivity.handleResponse(response))
                    return;

                mViewSwitcher.setDisplayedChild(1);

                Matcher m = SUBS_PATTERN.matcher(response.html);
                List<String> list = new ArrayList<String>();

                while(m.find()) {
                    list.add(m.group(1));
                }

                mAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_list_item_single_choice, list);
                mListView.setAdapter(mAdapter);
                setSelected();
                mActivity.supportInvalidateOptionsMenu();
            }
        };
        req.execute("https://www.youfone.nl/myYF/page.php");
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        boolean idle = mViewSwitcher != null && mViewSwitcher.getDisplayedChild() == 1;
        MenuItem refreshItem = menu.add(R.string.refresh)
                .setIcon(R.drawable.ic_action_refresh)
                .setOnMenuItemClickListener(this);
        refreshItem.setEnabled(idle);
        refreshItem.setVisible(idle);

        MenuItemCompat.setShowAsAction(refreshItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onNothingSelected(AdapterView<?> adapterView) {}

    // Refresh
    public boolean onMenuItemClick(MenuItem menuItem) {
        refresh();
        return true;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        mPrefs.edit().putInt(Const.PREF_COOKIE_NR, i).commit();
        mActivity.reload();
    }
}
