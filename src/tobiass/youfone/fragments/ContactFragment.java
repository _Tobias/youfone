package tobiass.youfone.fragments;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tobiass.youfone.Const;
import tobiass.youfone.ErrorDismissWatcher;
import tobiass.youfone.Main;
import tobiass.youfone.R;
import tobiass.youfone.YFRequest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class ContactFragment extends Fragment implements OnMenuItemClickListener {
	ViewSwitcher mViewSwitcher;
	Spinner mSubjectSpinner;
	Spinner mNumberSpinner;
	EditText mQuestion;
	SparseArray<String> subjects;
	List<Long> numbers;
	String postString;

    public void reset() {
        subjects = null;
        numbers = null;
    }
	
	static Pattern selectPattern = Pattern.compile("<select name=\"(.+?)\".*?>(.*?)</select>", Pattern.DOTALL);
	static Pattern optionPattern = Pattern.compile("<option value=\"([0-9]+)\".*?>(.+?)</option>");
	static Pattern inputPattern = Pattern.compile("<input.+?name=\"(.+?)\".+?value=\"(.+?)\"");
	public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		mViewSwitcher = (ViewSwitcher) inflater.inflate(R.layout.fragment_contact, null);
		mViewSwitcher.setDisplayedChild(0);
		
		mSubjectSpinner = (Spinner) mViewSwitcher.findViewById(R.id.input_subject);
		mNumberSpinner = (Spinner) mViewSwitcher.findViewById(R.id.input_number);
		mQuestion = (EditText) mViewSwitcher.findViewById(R.id.input_question);
		mQuestion.addTextChangedListener(new ErrorDismissWatcher(mQuestion));
		
		if(subjects == null) {
			YFRequest req = new YFRequest(getActivity()) {
				public void onPostExecute(YFResponse response) {
					mViewSwitcher.setDisplayedChild(1);
					
					Main activity = (Main) getActivity();
					
					if(activity == null || activity.handleResponse(response))
						return;
					
					subjects = new SparseArray<String>();
					numbers = new ArrayList<Long>();
					postString = "";
					
					Matcher selectMatcher = selectPattern.matcher(response.html);
					while(selectMatcher.find()) {
						String fieldName = selectMatcher.group(1);
						String optionHtml = selectMatcher.group(2);
						
						Matcher optionMatcher = optionPattern.matcher(optionHtml);
						while(optionMatcher.find()) {
							String value = optionMatcher.group(1);
							String name = optionMatcher.group(2);
							
							if(fieldName.equals("TicketType"))
								subjects.append(Integer.valueOf(value), name);
							else if(fieldName.equals("Telefoon"))
								numbers.add(Long.valueOf(value));
						}
					}
					
					Matcher inputMatcher = inputPattern.matcher(response.html);
					while(inputMatcher.find())
						try {
							postString += inputMatcher.group(1)+"="+URLEncoder.encode(inputMatcher.group(2), Const.CHARSET)+"&";
						} catch (UnsupportedEncodingException e) {}
					
					load();
				}
			};
			req.execute("https://www.youfone.nl/myYF/page.php?id=5");
		}
		else
			load();
		return mViewSwitcher;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	private void load() {
		mSubjectSpinner.setAdapter(new SparseAdapter(getActivity(), subjects));
		mNumberSpinner.setAdapter(new SparseAdapter(getActivity(), numbers));
		
		mViewSwitcher.setDisplayedChild(1);
		getActivity().supportInvalidateOptionsMenu();
	}
	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		boolean idle = mViewSwitcher != null && mViewSwitcher.getDisplayedChild() == 1;
		MenuItem refreshItem = menu.add(R.string.send)
				.setIcon(R.drawable.ic_action_send_now)
				.setVisible(idle)
				.setEnabled(idle)
				.setOnMenuItemClickListener(this);
		
		MenuItemCompat.setShowAsAction(refreshItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
	    super.onCreateOptionsMenu(menu, inflater);
	}

	// send message
	public boolean onMenuItemClick(MenuItem item) {
		String msg = mQuestion.getText().toString().trim();
		YFRequest req = new YFRequest(getActivity()) {
			public void onPostExecute(YFResponse response) {
				mQuestion.setText(null);
				mViewSwitcher.setDisplayedChild(1);
				getActivity().supportInvalidateOptionsMenu();
				Toast.makeText(getActivity(), R.string.message_question_sent, Toast.LENGTH_LONG).show();
			}
		};
		if(msg.length() == 0) {
			mQuestion.setError(getText(R.string.error_question_too_short));
			return true;
		}
		try {
			req.execute("https://www.youfone.nl/myYF/page.php?id=5", postString+String.format("TicketType=%s&Telefoon=%s&Description=%s", subjects.valueAt(mSubjectSpinner.getSelectedItemPosition()), mNumberSpinner.getSelectedItem(), URLEncoder.encode(msg, Const.CHARSET)));
			mViewSwitcher.setDisplayedChild(0);
			getActivity().supportInvalidateOptionsMenu();
		} catch (UnsupportedEncodingException e) {}
		return true;
	}
}
