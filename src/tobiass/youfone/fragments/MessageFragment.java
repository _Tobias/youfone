package tobiass.youfone.fragments;

import tobiass.youfone.Const;
import tobiass.youfone.Main;
import tobiass.youfone.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MessageFragment extends Fragment implements OnClickListener {
	public View onCreateView(final LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_message, null);
		TextView textView = (TextView) viewGroup.findViewById(android.R.id.text1);
		Button button = (Button) viewGroup.findViewById(R.id.action_button);
		Bundle args = getArguments();
		textView.setText(args.getInt(Const.ARGUMENT_MESSAGE));
		if(args.containsKey(Const.ARGUMENT_BUTTON_TITLE) && args.containsKey(Const.ARGUMENT_BUTTON_ACTION)) {
			button.setVisibility(View.VISIBLE);
			button.setText(args.getString(Const.ARGUMENT_BUTTON_TITLE));
			button.setTag(args.getInt(Const.ARGUMENT_BUTTON_ACTION));
			button.setOnClickListener(this);
		}
		return viewGroup;
	}

	public void onClick(View v) {
		Integer action = (Integer) v.getTag();
		Main activity = (Main) getActivity();
		switch(action.intValue()) {
		case Const.ACTION_LOGOUT:
			activity.logout();
			break;
		}
	}
}
